using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroiScript : MonoBehaviour
{

    public float velocity;
    private Rigidbody2D body;
    private Animator animator;

    void Start()
    {
        this.body = GetComponent<Rigidbody2D>();
        this.body.isKinematic = true;
        this.animator = GetComponent<Animator>();
    
        this.animator.enabled = false;
    }

    void Update()
    {
        float x = this.transform.position.x;
        float y = this.transform.position.y;
        float movimentKey = Input.GetAxis("Horizontal");

        if (movimentKey > 0)
        {
            this.body.MovePosition(new Vector2(x + this.velocity, y));
            this.animator.enabled = true;
        }
        else if (movimentKey < 0)
        {
            this.body.MovePosition(new Vector2(x - this.velocity, y));
            this.animator.enabled = true;
        }
        else
        {
            this.body.MovePosition(new Vector2(x, y));
             this.animator.enabled = false;   
        }
        

    }
}
